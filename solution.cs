public class Solution
{
    //Test1 - A Very Big Sum

    public static long big_Sum(int[] numberstototal)
    {
        long total = 0;
        int i, n = 10;

        for (i = 1; i <= n; i++)
        {
            total = total + Convert.ToInt64(numberstototal[i]);
        }

        return total;
    }


    //Test2 - Time Conversion

    public static string converted_time(string time)
    {
        if (time.contains("AM"))
        {
            time = time.replace("AM", "");
            String[] timeArr = time.split(":");
            if (timeArr[0].equals("12"))
            {
                timeArr[0] = "00";
            }
            time = timeArr[0] + ":" + timeArr[1] + ":" + timeArr[2];
        }
        else if (time.contains("PM"))
        {
            time = time.replace("PM", "");
            String[] timeArr = time.split(":");
            if (!timeArr[0].equals("12"))
            {
                timeArr[0] = Integer.toString(Integer.parseInt(timeArr[0]) + 12);
            }
            time = timeArr[0] + ":" + timeArr[1] + ":" + timeArr[2];
        }

        return time;
    }
}